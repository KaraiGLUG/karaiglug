Rails.application.routes.draw do
  get 'gallery/index'

  get 'archives/index'

  devise_for :users, skip: :registrations
  as :user do
  	get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'    
  	put 'users' => 'devise/registrations#update', :as => 'user_registration'            
  end
  
  resources :posts do
  	 resources :comments
  end
  root 'welcome#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
