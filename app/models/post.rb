class Post < ApplicationRecord
	mount_uploader :image, ImageUploader
	has_many :comments
	def day
		self.created_at.strftime("%B %Y")
    end
end
