class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
   	def author? 
	  self.role == 'author' 
	end
	
	def admin? 
	  self.role == 'admin' 
	end
end
