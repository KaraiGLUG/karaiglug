module ApplicationHelper
	def kramdown(text)
  		return sanitize Kramdown::Document.new(text).to_html
	end

	def full_title(page_title = '')
    base_title = "Karai-GLUG"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

end
