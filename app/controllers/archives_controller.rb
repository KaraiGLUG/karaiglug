class ArchivesController < ApplicationController
  def index
  	@posts = Post.all.order('created_at DESC').group_by(&:day)
  end
end
